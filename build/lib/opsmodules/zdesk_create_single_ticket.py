#!/usr/local/bin/python3
from zdesk import Zendesk


def generate_ticket(api_key, subject, description):
    # Define common variables
    requester_email = "rd-test@coveo.com"
    requester_name = "Ops Internal"
    tags = ["automated", "ops", "internal"]
    type = "task"

    config = {
        "zdesk_email": "maudet@coveo.com",
        "zdesk_password": api_key,
        "zdesk_url": "https://coveo.zendesk.com",
        "zdesk_token": True,
    }

    # Auth
    zendesk = Zendesk(**config)

    # Define ticket
    new_ticket = {
        "ticket": {
            "requester": {"name": requester_name, "email": requester_email},
            "subject": subject,
            "description": description,
            "tags": tags,
            "type": type,
            "priority": "normal",
            "group_id": 24712758,
            "custom_fields": [{"id": 25306187, "value": "production_stratus"}, {"id": 25306197, "value": "None"}],
        }
    }

    result = zendesk.ticket_create(data=new_ticket)

