#!/usr/local/bin/python3
import gzip

import boto3
import click


def upload_file_to_s3(s3_bucket, s3_key, file_name):
    # Removed gzip functionality, it seemed to mess with the upload
    click.echo("Uploading {} to S3".format(file_name))
    try:
        content = open(file_name, "rb")
        s3 = boto3.client("s3")
        s3.put_object(Bucket=s3_bucket, Key=s3_key + "/" + file_name, ServerSideEncryption="AES256", Body=content)
    except Exception as e:
        click.echo(e)

