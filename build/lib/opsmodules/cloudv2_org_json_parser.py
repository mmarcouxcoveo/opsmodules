def populate_org_dict(jason, org_dict):
    """Populate an org dictionary with useful information from the CloudV2 Platform JSON"""
    for key, value in jason.items():
        if key == "items":
            for item in value:
                org_id = ""
                provisioned = False

                try:
                    id_dict = {"id": v for k, v in item.items() if k == "id"}
                    license_dict = {"license": v for k, v in item.items() if k == "license"}
                    prov_dict = {"provisioningStatus": v for k, v in item.items() if k == "provisioningStatus"}
                    org_id = id_dict["id"]
                    license_type = [i["productType"] for i in license_dict.values()][0]
                    index_type = [i["indexType"] for i in license_dict.values()][0]
                    init_prov_done = [i["initialProvisioningDone"] for i in prov_dict.values()][0]
                    init_prov_progress = [i["currentProvisioningProgress"] for i in prov_dict.values()][0]

                except KeyError as e:
                    print(f"ERROR: KeyError on {org_id}... {e}")
                    continue
                except IndexError as e:
                    print(f"ERROR: IndexError on {org_id}... {e}")
                    continue

                if init_prov_done and init_prov_progress == 100:
                    provisioned = True

                if org_id and license_type and index_type and org_id not in org_dict:
                    org_dict[org_id] = {"license": license_type, "index_type": index_type, "provisioned": provisioned}

