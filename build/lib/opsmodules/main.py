#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = "1.0"

import boto3


def asgclient(region):
    asgclient = boto3.client("autoscaling", region_name=region)
    return asgclient


def dynamodbclient(region):
    dynamoclient = boto3.client("dynamodb", region_name=region)
    return dynamoclient


def ec2client(region):
    ec2client = boto3.client("ec2", region_name=region)
    return ec2client


def ecsclient(region):
    ecsclient = boto3.client("ecs", region_name=region)
    return ecsclient


def elasticclient(region):
    elasticclient = boto3.client("elasticbeanstalk", region_name=region)
    return elasticclient


def iamclient(region):
    iamclient = boto3.client("iam", region_name=region)
    return iamclient


def lambdaclient(region):
    lambdaclient = boto3.client("lambda", region_name=region)
    return lambdaclient


def kmsclient(region):
    kmsclient = boto3.client("kms", region_name=region)
    return kmsclient


def opsworksclient(region):
    opsworksclient = boto3.client("opsworks", region_name=region)
    return opsworksclient


def rdsclient(region):
    rdsclient = boto3.client("rds", region_name=region)
    return rdsclient


def redshiftclient(region):
    redshiftclient = boto3.client("redshift", region_name=region)
    return redshiftclient


def r53client(region):
    r53client = boto3.client("route53", region_name=region)
    return r53client


def s3client(region):
    s3client = boto3.client("s3", region_name=region)
    return s3client


def ssmclient(region):
    ssmclient = boto3.client("ssm", region_name=region)
    return ssmclient

