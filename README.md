# OPSModules

OPS Modules

To use, import one of these modules:
aws_to_platformdb_architecture - Usability of AWS + Platform naming convention of instance types
boto_clients - Most of the BOTO3 clients used by OPS
cloudv2_ops_client - Client made by Joseph-Antoine Cloutier to handle multiple tasks and features of the platforms from API Calls, Tokens management, Resize Orgs, etc...
cloudv2_org_json_parser - Populate an org dictionary with useful information from the CloudV2 Platform JSON
upload_file_to_s3 - Upload file to S3
zdesk_create_single_ticket - Create Zendesk ticket easily

Example : import opsmodules.boto_clients as clients or from opsmodules import boto_clients as clients