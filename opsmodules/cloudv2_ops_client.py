import click
import requests
from ratelimit import limits, sleep_and_retry
from requests.auth import HTTPBasicAuth

HOSTNAMES = {"dev": "platformdev", "qa": "platformqa", "prod": "platform", "hipaa": "platformhipaa"}


class OpsClient:
    """
    A client to help automate interactions with the CloudV2 platform
    """

    def __init__(self, environment, auth_id, auth_password, ops_token, session_object=None):
        """
        Default to ops_token at first.
        Auth with ClientID available but must be manually triggered
        """
        self._ops_token = ops_token
        self._ops_token_in_use = True
        self._token = ops_token
        self._headers = None
        self._session = None
        self._auth_id = auth_id
        self._auth_password = auth_password
        self._environment = environment
        self._hostname = HOSTNAMES[environment]
        self._protocol = "https"
        self._platform_url = ""
        self._platform_auth_url = ""
        self._platform_search_url = ""
        self.set_headers()
        self.set_session(session_object)
        self.set_platform_urls()

    def set_headers(self, token=None, session_object=None):
        """Update the token (in headers), and optionally refresh the session object with it"""
        self._headers = {
            "Authorization": f"Bearer {self._token if not token else token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
        if session_object:
            # When a session is passed here, update it's headers
            session_object.headers.update(self._headers)

    def set_session(self, session_object=None):
        """Create a session object, or update the headers on an existing one"""
        if not session_object:
            s = requests.Session()
        else:
            s = session_object
        s.headers.update(self._headers)
        self._session = s

    def set_platform_urls(self, protocol=None, hostname=None):
        """Refresh the URLs"""
        new_protocol = self._protocol if not protocol else protocol
        new_hostname = self._hostname if not hostname else hostname
        self._platform_url = f"{new_protocol}://{new_hostname}.cloud.coveo.com/rest/organizations"
        self._platform_auth_url = (
            f"{new_protocol}://{new_hostname}.cloud.coveo.com/oauth/token?grant_type=client_credentials"
        )
        self._platform_search_url = f"{new_protocol}://{new_hostname}.cloud.coveo.com/rest/search/v2/"

    def use_oauth2_client_id(self):
        """Attempts to authenticate with a ClientID/Password"""
        r = self._session.post(self._platform_auth_url, auth=HTTPBasicAuth(self._auth_id, self._auth_password))
        try:
            r.raise_for_status()
            token = r.json()["access_token"]
            self._ops_token_in_use = False
            print(f"[OPS CLIENT] - Successfully authenticated with {self._auth_id}")
        except requests.exceptions.HTTPError:
            if r.status_code >= 500:
                raise  # Let server errors through, I can't / won't handle them.
            if r.status_code == 401:
                # Bad password for the client id
                print(f"[OPS CLIENT] - Failed to authenticate with {self._auth_id} - Falling back to your ops token.")
                token = self._ops_token
                self._ops_token_in_use = True
            else:
                raise  # Something bad happened -_-'
        self._token = token
        self.set_headers(self._token, self._session)

    def renew_authentication(self):
        """Renew the token and headers"""
        if self._ops_token_in_use:
            print(
                "[OPS CLIENT] - Your ops token has expired and I can't renew it for you."
                + "\n[OPS CLIENT] - Prepare to burn in the fires of Mordor!"
            )
            exit(1)
        else:
            self.use_oauth2_client_id()

    def switch_to_ops_token(self):
        """Switch to the provided ops token"""
        if not self._ops_token_in_use:
            print("[OPS CLIENT] - Switching to your provided ops_token.")
            self._token = self._ops_token
            self.set_headers(self._token, self._session)
            self._ops_token_in_use = True
        else:
            print(f"[OPS CLIENT] - Got a 403 (Forbidden) and an ops token is already in use.")

    @sleep_and_retry
    @limits(calls=1, period=1)
    def call_platform_api(self, target_url, request_function, payload=None, params=None, data=None, auth_failures=0):
        """Generic wrapper for CloudV2 Platform API calls"""
        if auth_failures <= 4:
            if not payload and not params and not data:
                response = request_function(target_url)
            elif payload and not params and not data:
                response = request_function(target_url, json=payload)
            elif not payload and params and not data:
                response = request_function(target_url, params=params)
            elif payload and params and not data:
                response = request_function(target_url, json=payload, params=params)
            elif not payload and not params and data:
                response = request_function(target_url, data=data)
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError:
                if response.status_code >= 500:
                    raise  # Let server errors through, I can't / won't handle them.
                elif response.status_code == 401:
                    print("[OPS CLIENT] - Expired token. Renewing...")
                    self.renew_authentication()
                    response = self.call_platform_api(target_url, request_function, payload, params, data)
                elif response.status_code == 403:
                    if not self._ops_token_in_use:
                        print(f"[OPS CLIENT] - Got a 403 (Forbidden) with {self._auth_id}.")
                        self.switch_to_ops_token()
                    elif self._ops_token_in_use:
                        print(f"[OPS CLIENT] - Oh you dirty boy, we got a 403 (Forbidden) with your ops token...")
                    auth_failures += 1
                    response = self.call_platform_api(
                        target_url, request_function, payload, params, data, auth_failures
                    )
                elif response.status_code >= 400:
                    print(f"[OPS CLIENT] - Got a {response.status_code} on {target_url}, I hope you can handle it!")
            return response
        else:
            print(
                f"[OPS CLIENT] - Something's up, we've had {auth_failures} consecutive auth failures.\n"
                + "[OPS CLIENT] - You're on the naughty list. Exiting!"
            )
            exit(1)

    def pause_org(self, org_id):
        """Triggers pause on a single organization"""
        target = f"{self._platform_url}/{org_id}/pause"
        return self.call_platform_api(target, self._session.post)

    def resume_org(self, org_id):
        """Triggers resume on a single organization"""
        target = f"{self._platform_url}/{org_id}/resume"
        return self.call_platform_api(target, self._session.post)

    def trigger_upgrade(self, org_id):
        """Triggers a single organization upgrade"""
        target = f"{self._platform_url}/{org_id}/upgrade"
        return self.call_platform_api(target, self._session.post)

    def fetch_one_page(self, org_id, api_call, page=0, request_payload=None):
        """Returns a single page from a paginated platform API call"""
        target = f"{self._platform_url}/{org_id}/{api_call}?page={page}&perPage=50"
        if not request_payload:
            return self.call_platform_api(target, self._session.post)
        else:
            return self.call_platform_api(target, self._session.post, request_payload)

    def fetch_organization_page(self, page, options):
        """Fetch a single page of all organizations json"""
        target = f"{self._platform_url}?{options}&page={page}&perPage=50"
        return self.call_platform_api(target, self._session.get)

    def fetch_paginated_organizations(self, options):
        """Returns a list of all pages specifically for the /rest/organizations paginated call"""
        results = []
        current_page = 0
        total_pages = 0
        totals_are_set = False
        continue_iteration = True

        while continue_iteration:
            response_json = self.fetch_organization_page(current_page, options).json()

            # First time through, we grab this to track our progress
            if not totals_are_set:
                total_pages, totals_are_set = set_totals(response_json)

            results.append(response_json)
            echo_progress(len(results), total_pages, "Fetching pages")
            current_page += 1
            if current_page >= total_pages:
                continue_iteration = False

        return results

    def fetch_paginated(self, org_id, api_call, request_payload=None):
        """Returns a list of all pages for (almost) any paginated API call"""
        results = []
        current_page = 0
        total_pages = 0
        totals_are_set = False
        continue_iteration = True

        while continue_iteration:
            # The request_payload is expected to be a dict
            if not request_payload:
                response_json = self.fetch_one_page(org_id, api_call, current_page).json()
            else:
                response_json = self.fetch_one_page(org_id, api_call, current_page, request_payload).json()

            # First time through, we grab this to track our progress
            if not totals_are_set:
                total_pages, totals_are_set = set_totals(response_json)

            results.append(response_json)
            echo_progress(len(results), total_pages, "Fetching pages")
            current_page += 1
            if current_page >= total_pages:
                continue_iteration = False

        return results

    def fetch_cluster_information(self, org_id):
        """Fetch cluster information for a single organization"""
        target = f"{self._platform_url}/{org_id}/clusters"
        return self.call_platform_api(target, self._session.get)

    def fetch_index_statuses(self, org_id, index_id=""):
        """Fetch all indexes for a single organization, or a specific index"""
        target = (
            f"{self._platform_url}/{org_id}/indexes"
            if not index_id
            else f"{self._platform_url}/{org_id}/indexes/{index_id}"
        )
        return self.call_platform_api(target, self._session.get)

    def get_or_put_organization_license(self, org_id, data=None):
        """Get one org's license, or put it if a payload is received"""
        target = f"{self._platform_url}/{org_id}/license/internal"
        if not data:
            return self.call_platform_api(target, self._session.get)
        else:
            return self.call_platform_api(target, self._session.put, None, None, data)

    def delete_one_index(self, org_id, index_id):
        """Deletes a single index"""
        target = f"{self._platform_url}/{org_id}/indexes/{index_id}"
        return self.call_platform_api(target, self._session.delete)

    def change_index_online_or_offline(self, org_id, index_id, is_online):
        """Change an index's online/offline status. is_online should be True for online, False for offline"""
        assert isinstance(is_online, bool)
        target = f"{self._platform_url}/{org_id}/indexes/{index_id}/online?isOnline={'true' if is_online else 'false'}"
        return self.call_platform_api(target, self._session.put)

    def resize_index_in_place(self, org, agent, target_instance_size):
        """Resizes one index to a given target size (using the platform API, not boto)"""
        target = (
            f"{self._platform_url}/{org}/clusters/agents/{agent}/resize?force=true&instanceArchitecture={target_instance_size}"
        )
        return self.call_platform_api(target, self._session.post)

    def resize_index_by_copy(self, org_id, request_payload=None):
        """Trigger a copy from an existing indexer. A json payload is required."""
        target = f"{self._platform_url}/{org_id}/indexes"
        return self.call_platform_api(target, self._session.post, request_payload)

    def migrate_sec_prov(self, org_id):
        """Triggers migration to task mode and sec prov cluster on a single organization"""
        target = f"{self._platform_url}/{org_id}/migrate"
        return self.call_platform_api(target, self._session.post)

    def migrate_to_dual_pushapi_identities(self, org_id, cluster_id):
        """Triggers migration of poucheapi securities on a single organization"""
        target = f"{self._platform_url}/{org_id}/clusters/{cluster_id}/identitytargetmode?identityTargetMode=BOTH"
        return self.call_platform_api(target, self._session.post)

    def get_org_status(self, org_id):
        """Gets an org's status"""
        target = f"{self._platform_url}/{org_id}/status"
        return self.call_platform_api(target, self._session.get)

    def delete_organization(self, org_id):
        """Deletes an organization"""
        target = f"{self._platform_url}/{org_id}"
        return self.call_platform_api(target, self._session.delete)

    def send_query(self, params):
        """Query an organization. Requires valid params."""
        return self.call_platform_api(self._platform_search_url, self._session.get, None, params)


def echo_progress(current_pos, max_pos, action):
    """Boboche progress prints"""
    progress = float(current_pos)
    total = float(max_pos)
    try:
        percent = "%.2f" % float((progress / total) * 100)
        print(f"\n{action}: {percent}% --- {current_pos}/{max_pos}")
    except ZeroDivisionError as e:
        print(f"Can't echo progress: {e}")


def set_totals(json_to_parse):
    for key, value in json_to_parse.items():
        if key == "totalPages":
            return value, True

