#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = "1.0.1"


def main():
    print("Version: " + __version__)
    print("OPS Modules")
    print(
        "To use, import one of these modules:"
        + "\naws_to_platformdb_architecture, boto_clients, cloudv2_ops_client, cloudv2_org_json_parser, upload_file_to_s3, zdesk_create_single_ticket"
        + "\nExample: import opsmodules.boto_clients as clients"
    )


if __name__ == "__main__":
    main()
