#!/usr/bin/python3

# Accepted instance types
aws_to_platform_architecture = {
    "t3.small": "T3_SMALL_UNLIMITED",
    "t3.medium": "T3_MEDIUM_UNLIMITED",
    "t3.large": "T3_LARGE_UNLIMITED",
    "t3.xlarge": "T3_XLARGE_UNLIMITED",
    "t3.2xlarge": "T3_2XLARGE_UNLIMITED",
    "t3a.small": "T3A_SMALL_UNLIMITED",
    "t3a.medium": "T3A_MEDIUM_UNLIMITED",
    "t3a.large": "T3A_LARGE_UNLIMITED",
    "t3a.xlarge": "T3A_XLARGE_UNLIMITED",
    "t3a.2xlarge": "T3A_2XLARGE_UNLIMITED",
    "c5.large": "C5_LARGE",
    "c5.xlarge": "C5_XLARGE",
    "c5.2xlarge": "C5_2XLARGE",
    "c5.4xlarge": "C5_4XLARGE",
    "c5.9xlarge": "C5_9XLARGE",
    "c5.18xlarge": "C5_18XLARGE",
    "m5.large": "M5_LARGE",
    "m5.xlarge": "M5_XLARGE",
    "m5.2xlarge": "M5_2XLARGE",
    "m5.4xlarge": "M5_4XLARGE",
    "m5.12xlarge": "M5_12XLARGE",
    "m5.24xlarge": "M5_24XLARGE",
    "m5a.large": "M5A_LARGE",
    "m5a.xlarge": "M5A_XLARGE",
    "m5a.2xlarge": "M5A_2XLARGE",
    "m5a.4xlarge": "M5A_4XLARGE",
    "m5a.12xlarge": "M5A_12XLARGE",
    "m5a.24xlarge": "M5A_24XLARGE",
    "r5.large": "R5_LARGE",
    "r5.xlarge": "R5_XLARGE",
    "r5.2xlarge": "R5_2XLARGE",
    "r5.4xlarge": "R5_4XLARGE",
    "r5.12xlarge": "R5_12XLARGE",
    "NOT_PASSED": "NOT_PASSED",
}

# Convert aws to platform instance type
def set_architecture_config(aws_target_type):
    try:
        coveo_target_type = aws_to_platform_architecture[aws_target_type]
        return coveo_target_type
    except KeyError:
        print(f"Sorry, no match found for this target configuration ({aws_target_type})")
        print("Please add the proper key to 'aws_to_platform_architecture' in the script")
        exit(1)

